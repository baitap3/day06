CREATE DATABASE IF not EXISTS ltweb;

USE ltweb;

-- DROP TABLE `SINHVIEN`;
CREATE Table IF not EXISTS
    SINHVIEN(
        ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
        HoVaTenSV VARCHAR(30),
        GioiTinh char(1),
        MaKH VARCHAR(6),
        NgaySinh DATE,
        DiaChi VARCHAR(50),
        HinhAnh VARCHAR(100)
    );

SELECT * FROM SINHVIEN;